//
//  MovieCell.swift
//  EMDb
//
//  Created by Xavier Criado Carmona on 24/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

import UIKit

class MovieCell: UICollectionViewCell {
    
    @IBOutlet weak var movieImage : UIImageView!
    
}
