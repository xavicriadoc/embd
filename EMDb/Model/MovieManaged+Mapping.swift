//
//  MovieManaged+Mapping.swift
//  EMDb
//
//  Created by Xavier Criado Carmona on 24/01/2018.
//  Copyright © 2018 Xavier Criado Carmona. All rights reserved.
//

import Foundation

extension MovieManaged {
    
    func mappedObject() -> Movie{
        return Movie(id: self.id, title: self.title, order: Int(self.order), summary: self.summary, image: self.image, category: self.category, director: self.director)
    }
    
}
